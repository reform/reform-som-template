# MNT Reform SoM Template for KiCAD

The processor and memory in the MNT Reform open hardware laptop sit on a plug-in module in SODIMM 200 format. The template in this repository allows you to get started quickly with alternative processor/memory modules.

Note that this is currently in an unverified alpha state. More information will follow.

![MNT Reform SoM Template Screenshot](reform-som-template-screenshot.png)
